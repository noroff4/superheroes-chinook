﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superheroes_Assignment.Repositories
{
    public static class NullValuesHelper
    {
        public static string SafeGetString(this SqlDataReader reader, int colIndex)
        {
            return reader.IsDBNull(colIndex) ? string.Empty : reader.GetString(colIndex);
        }
    }
}
