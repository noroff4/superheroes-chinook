﻿using Microsoft.Data.SqlClient;
using Superheroes_Assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superheroes_Assignment.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = NullValuesHelper.SafeGetString(reader, 4);
                                temp.Phone = NullValuesHelper.SafeGetString(reader, 5);
                                temp.Email = NullValuesHelper.SafeGetString(reader, 6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }
            return customerList;
        }

        public List<Customer> GetLimitedCustomers(int limit, int offset)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "ORDER BY CustomerId " +
                "OFFSET @Offset ROWS " + 
                "FETCH NEXT @Limit ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset-1);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.GetString(3);
                                temp.PostalCode = NullValuesHelper.SafeGetString(reader, 4);
                                temp.Phone = NullValuesHelper.SafeGetString(reader, 5);
                                temp.Email = NullValuesHelper.SafeGetString(reader, 6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }
            return customerList;
        }



        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE CustomerId = @CustomerId";

            try
            {
                using(SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using(SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = NullValuesHelper.SafeGetString(reader, 1);
                                customer.LastName = NullValuesHelper.SafeGetString(reader, 2);
                                customer.Country = NullValuesHelper.SafeGetString(reader, 3);
                                customer.PostalCode = NullValuesHelper.SafeGetString(reader, 4);
                                customer.Phone = NullValuesHelper.SafeGetString(reader, 5);
                                customer.Email = NullValuesHelper.SafeGetString(reader, 6);
                            }
                        }
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }

            return customer;
        }

        public Customer GetCustomerByName(string name)
        {
            Customer customer = new Customer();

            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
    " WHERE FirstName LIKE '%' + @Search + '%' OR LastName LIKE '%' + @Search + '%'";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Search", name);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = NullValuesHelper.SafeGetString(reader, 1);
                                customer.LastName = NullValuesHelper.SafeGetString(reader, 2);
                                customer.Country = NullValuesHelper.SafeGetString(reader, 3);
                                customer.PostalCode = NullValuesHelper.SafeGetString(reader, 4);
                                customer.Phone = NullValuesHelper.SafeGetString(reader, 5);
                                customer.Email = NullValuesHelper.SafeGetString(reader, 6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }




            return customer;
        }


        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "SET IDENTITY_INSERT Customer ON " +
                "INSERT INTO Customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@CustomerId, @FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";


            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", GetMaxId()+1);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }
            return success;
        }

        public int GetMaxId()
        {
            string sql = "SELECT MAX(CustomerId) as Max_Id FROM Customer";
            int max = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                max = reader.GetInt32(0);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }
            return max;
        }

        public bool UpdateCustomer(Customer customer, int id)
        {
            bool success = false;
            string sql = "UPDATE Customer " +
                "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                "WHERE CustomerId = @ID";



            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        cmd.Parameters.AddWithValue("@ID", id);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }
            return success;
        }

        public List<CustomerCountry> GetCustomerCountries()
        {
            var countries = new List<CustomerCountry>();

            string sql = "SELECT COUNT(CustomerId) as [Count], Country " +
                "FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY Count DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry temp = new CustomerCountry();
                                temp.amount = reader.GetInt32(0);
                                temp.country = reader.GetString(1);
                                countries.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }

            return countries;
        }

        public List<CustomerSpender> GetCustomerSpenders()
        {
            var spenders = new List<CustomerSpender>();

            string sql = "SELECT Customer.FirstName, Customer.LastName, SUM(Invoice.Total) AS TotalInvoice " +
                "FROM Customer " + 
                "INNER JOIN Invoice " + 
                "ON Invoice.CustomerId = Customer.CustomerId " + 
                "GROUP BY Customer.FirstName, Customer.LastName " +
                "ORDER BY TotalInvoice DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.CustomerFullName = $"{reader.GetString(0)} {reader.GetString(1)}";
                                temp.InvoiceTotal = reader.GetDecimal(2);
                                spenders.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }

            return spenders;
        }

        public CustomerGenre GetCustomerFavouriteGenre(int id)
        {
            var favGenre = new CustomerGenre();

            string sql = "SELECT TOP 1 Customer.CustomerId, Genre.GenreId, COUNT(Genre.GenreId) AS GenrePopularity, Genre.[Name] " + 
                "From Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                "INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId " +
                "INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                "WHERE Customer.CustomerId = @ID " +
                "GROUP BY Customer.CustomerId, Genre.GenreId, Genre.[Name] " +
                "ORDER BY Customer.CustomerId, GenrePopularity DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                favGenre.Genre = reader.GetString(3);
                                favGenre.Popularity = reader.GetInt32(2);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("--------------------");
                Console.WriteLine(ex.ErrorCode);
            }

            return favGenre;
        }


    }
}
