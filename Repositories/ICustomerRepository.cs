﻿using Superheroes_Assignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superheroes_Assignment.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomer(int id);
        public Customer GetCustomerByName(string name);
        public List<Customer> GetAllCustomers();
        public List<Customer> GetLimitedCustomers(int limit, int offset);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer, int id);
        public List<CustomerCountry> GetCustomerCountries();
        public List<CustomerSpender> GetCustomerSpenders();
        public CustomerGenre GetCustomerFavouriteGenre(int id);
        public int GetMaxId();
    }
}
