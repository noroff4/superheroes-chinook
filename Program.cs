﻿using Superheroes_Assignment.Repositories;
using Superheroes_Assignment.Models;

namespace Superheroes_Assignment
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository repository = new CustomerRepository();
            bool running = true;
            while(running) // MAIN MENU
            {
                Console.Clear();
                Console.WriteLine("1) All Customers");
                Console.WriteLine("2) Customer by ID");
                Console.WriteLine("3) Customer by Name");
                Console.WriteLine("4) X Amount of customers starting at Y");
                Console.WriteLine("5) Add Customer");
                Console.WriteLine("6) Update Customer");
                Console.WriteLine("7) # of Customers in each country");
                Console.WriteLine("8) Highest spenders");
                Console.WriteLine("9) Find the most popular genre by Customer Id");
                Console.WriteLine("10) Exit");
                var userOption = Console.ReadLine();
                switch (userOption)
                {
                    case "1":
                        SelectAllCustomers(repository);
                        break;
                    case "2":
                        SelectCustomer(repository);
                        break;
                    case "3":
                        SelectCustomerByName(repository);
                        break;
                    case "4":
                        SelectLimitedCustomers(repository);
                        break;
                    case "5":
                        InsertCustomer(repository);
                        break;
                    case "6":
                        UpdateCustomer(repository);
                        break;
                    case "7":
                        PrintCountryCount(repository);
                        break;
                    case "8":
                        PrintTotalInvoices(repository);
                        break;
                    case "9":
                        PrintFavouriteGenre(repository);
                        break;
                    case "10":
                        running = false;
                        break;
                    default:
                        Console.WriteLine("Option not valid!");
                            break;
                }
            }
        }

        /// <summary>
        /// Gets all customers from <paramref name="repository"/>
        /// </summary>
        /// <param name="repository"></param>
        static void SelectAllCustomers(ICustomerRepository repository)
        {
            Console.Clear();
            PrintCustomers(repository.GetAllCustomers());
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }
        /// <summary>
        /// Gets specific customer from <paramref name="repository"/>
        /// </summary>
        /// <param name="repository"></param>
        static void SelectCustomer(ICustomerRepository repository)
        {
            Console.Clear();
            Console.WriteLine("Id:");
            var id = Convert.ToInt32(Console.ReadLine());
            if(id>repository.GetMaxId())
            {
                Console.WriteLine("Id does not exist");
            }
            else
            {
                PrintCustomer(repository.GetCustomer(id));
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }
        /// <summary>
        /// Prints custom amount of customers starting at specific point
        /// </summary>
        /// <param name="repository"></param>
        static void SelectLimitedCustomers(ICustomerRepository repository)
        {
            Console.Clear();
            Console.WriteLine("Amount:");
            var limit = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Starting position:");
            var offset = Convert.ToInt32(Console.ReadLine());
            var customers = repository.GetLimitedCustomers(limit, offset);
            if (customers.Count > 0)
            {
                PrintCustomers(customers);
            }
            else
            {
                Console.WriteLine("Customers not found.");
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }
        /// <summary>
        /// Prints the matching customer from input
        /// </summary>
        /// <param name="repository"></param>
        static void SelectCustomerByName(ICustomerRepository repository)
        {
            Console.Clear();
            Console.WriteLine("Search for a name:");
            var name = Console.ReadLine();
            var customer = repository.GetCustomerByName(name);
            if ( customer.CustomerId != default)
            {
                PrintCustomer(customer);
            }
            else
            {
                Console.WriteLine("Name does not exist.");
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }
        /// <summary>
        /// Add customer to <paramref name="repository"/>
        /// </summary>
        /// <param name="repository"></param>
        static void InsertCustomer(ICustomerRepository repository)
        {
            Console.Clear();
            var newCustomer = GetNewCustomer();

            if (repository.AddNewCustomer(newCustomer))
            {
                Console.WriteLine("Insert worked!");
            } 
            else
            {
                Console.WriteLine("Insert did not work!");
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }
        /// <summary>
        /// Update a customer from <paramref name="repository"/>
        /// </summary>
        /// <param name="repository"></param>
        static void UpdateCustomer(ICustomerRepository repository)
        {
            Console.Clear();
            Console.WriteLine("Which user you want to update? Please give an id");
            var userId = Convert.ToInt32(Console.ReadLine());
            var temp = repository.GetCustomer(userId);
            PrintCustomer(temp);
            Console.WriteLine("Which property to update?");
            Console.WriteLine("1) First Name");
            Console.WriteLine("2) Last Name");
            Console.WriteLine("3) Country");
            Console.WriteLine("4) Postal Code");
            Console.WriteLine("5) Email");
            Console.WriteLine("6) Phone");
            var option = Console.ReadLine();
            switch (option)
            {
                case "1":
                    Console.WriteLine("Enter new first name:");
                    temp.FirstName = Console.ReadLine();
                    break;
                case "2":
                    Console.WriteLine("Enter new last name:");
                    temp.LastName = Console.ReadLine();
                    break;
                case "3":
                    Console.WriteLine("Enter new country:");
                    temp.Country = Console.ReadLine();
                    break;
                case "4":
                    Console.WriteLine("Enter new Postal Code:");
                    temp.PostalCode = Console.ReadLine();
                    break;
                case "5":
                    Console.WriteLine("Enter new Email:");
                    temp.Email = Console.ReadLine();
                    break;
                case "6":
                    Console.WriteLine("Enter new phone number:");
                    temp.Phone = Console.ReadLine();
                    break;
                default:
                    break;
            }

            repository.UpdateCustomer(temp, userId);
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }

        /// <summary>
        /// Prints out countries and its amount
        /// </summary>
        /// <param name="repository"></param>
        static void PrintCountryCount(ICustomerRepository repository)
        {
            Console.Clear();
            foreach(var country in repository.GetCustomerCountries())
            {
                Console.WriteLine(country);
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }
        /// <summary>
        /// Prints out total invoices on every customer
        /// </summary>
        /// <param name="repository"></param>
        static void PrintTotalInvoices(ICustomerRepository repository)
        {
            Console.Clear();
            foreach (var person in repository.GetCustomerSpenders())
            {
                Console.WriteLine(person);
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }

        /// <summary>
        /// Prints out all Customers
        /// </summary>
        /// <param name="customers"></param>
        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach(Customer customer in customers)
            {
                PrintCustomer(customer);
            }

        }
        /// <summary>
        /// Prints one customer
        /// </summary>
        /// <param name="customer"></param>
        static void PrintCustomer(Customer customer)
        {
            
            Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} ---");
            Console.WriteLine($"--- {customer.PostalCode} {customer.Email} {customer.Phone} ---");
            Console.WriteLine("----------------------------------------------------------------");
        }
        /// <summary>
        /// Asks user for a new customer
        /// </summary>
        /// <returns></returns>
        static Customer GetNewCustomer()
        {
            var customer = new Customer();
            Console.WriteLine("First name:");
            customer.FirstName = Console.ReadLine();
            Console.WriteLine("Last name:");
            customer.LastName = Console.ReadLine();
            Console.WriteLine("Country:");
            customer.Country = Console.ReadLine();
            Console.WriteLine("PostalCode:");
            customer.PostalCode = Console.ReadLine();
            Console.WriteLine("Email:");
            customer.Email = Console.ReadLine();
            Console.WriteLine("Phone:");
            customer.Phone = Console.ReadLine();
            return customer;
        }
        /// <summary>
        /// Prints out Favourite genre by Customer Id
        /// </summary>
        /// <param name="repository"></param>
        static void PrintFavouriteGenre(ICustomerRepository repository)
        {
            Console.Clear();
            Console.WriteLine("Enter an id:");
            var id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(repository.GetCustomerFavouriteGenre(id));
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();
        }
    }
}

