﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superheroes_Assignment.Models
{
    public class CustomerSpender
    {
        public string CustomerFullName { get; set; }
        public decimal InvoiceTotal { get; set; }
        public override string? ToString()
        {
            return $"{CustomerFullName}: {InvoiceTotal}";
        }
    }
}
