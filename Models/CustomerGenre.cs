﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Superheroes_Assignment.Models
{
    public class CustomerGenre
    {
        public string Genre { get; set; }
        public int Popularity { get; set; }

        public override string ToString()
        {
            return $"Favourite Genre: {Genre}, Total amount of songs: {Popularity}";
        }
    }
}
