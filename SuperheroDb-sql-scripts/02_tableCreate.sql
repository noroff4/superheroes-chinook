USE [Superheroes]
GO

CREATE TABLE Superhero (
	Id int IDENTITY(1,1) PRIMARY KEY,
	[Name] varchar(255) NOT NULL,
	Alias varchar(255),
	Origin varchar(255)
);

CREATE TABLE Assistant (
	Id int IDENTITY(1,1) PRIMARY KEY,
	[Name] varchar(255) NOT NULL,
);

CREATE TABLE [Power] (
	Id int IDENTITY(1,1) PRIMARY KEY,
	[Name] varchar(255) NOT NULL,
	[Description] varchar(255)
);