USE [Superheroes]
GO


ALTER TABLE Assistant
ADD SuperheroId int;

ALTER TABLE Assistant
ADD FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id);