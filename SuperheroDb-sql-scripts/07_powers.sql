USE [Superheroes]
GO

INSERT INTO [Power] ([Name], [Description]) VALUES ('Moshpit', 'Deal massive amount of damage by running in circles');
INSERT INTO [Power] ([Name], [Description]) VALUES ('VIOLENCE, SPEED, MOMENTUM', 'Unleash epic force');
INSERT INTO [Power] ([Name], [Description]) VALUES ('BOOM','Snipe');
INSERT INTO [Power] ([Name], [Description]) VALUES ('Sneak attack','Attack from the shadows, dealing increased damage');

INSERT INTO [Superhero_Power] ([SuperheroId],[PowerId]) VALUES ((SELECT Id FROM Superhero WHERE Alias = 'Dr. Disrespect'), (SELECT Id From [Power] WHERE [Name] = 'VIOLENCE, SPEED, MOMENTUM'));
INSERT INTO [Superhero_Power] ([SuperheroId],[PowerId]) VALUES ((SELECT Id FROM Superhero WHERE Alias = 'Dr. Disrespect'), (SELECT Id From [Power] WHERE [Name] = 'BOOM'));
INSERT INTO [Superhero_Power] ([SuperheroId],[PowerId]) VALUES ((SELECT Id FROM Superhero WHERE Alias = 'S��mies'), (SELECT Id From [Power] WHERE [Name] = 'Moshpit'));
INSERT INTO [Superhero_Power] ([SuperheroId],[PowerId]) VALUES ((SELECT Id FROM Superhero WHERE Alias = 'Dr. Disrespect'), (SELECT Id From [Power] WHERE [Name] = 'Sneak attack'));
INSERT INTO [Superhero_Power] ([SuperheroId],[PowerId]) VALUES ((SELECT Id FROM Superhero WHERE Alias = 'S��mies'), (SELECT Id From [Power] WHERE [Name] = 'Sneak attack'));

