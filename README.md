# Superheroes/Chinook-Database Assignment
## Description
This is a program used to query data from a SQL database. The project also contains scripts to create a database. This project was created for Noroff .NET fullstack accelerate course.
## How to use
To create superhero database execute all sql scripts in order in Microsof SQL server management studio. All sql scripts are located in the the folder SuperheroDB-sql-scripts
To use the program that queries the Chinook customer database, first run the script located in ChinookDB-sql-scripts folder in Microsoft SQL server management studio. Then change the connection string builder datasource in Repositories/ConnectionStringHelper.cs GetConnectionString() method to the server what you are using. To use the program run the project and follow on screen insturctions.
## Contributors
Leevi Peltonen and Mikko Kaipainen.
